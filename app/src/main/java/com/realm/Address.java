package com.realm;

import io.realm.RealmObject;

public class Address extends RealmObject {

  private String street;
  private String postcode;

  public Address() {}

  public Address(String street, String postcode) {
    this.street = street;
    this.postcode = postcode;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }
}
