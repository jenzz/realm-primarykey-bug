package com.realm;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import io.realm.Realm;

public class MainActivity extends ActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Realm realm = Realm.getInstance(this);
    realm.beginTransaction();

    Address address = new Address("123 456", "High street");

    User user1 = new User(1, address);
    realm.copyToRealmOrUpdate(user1);

    // Note: Same id and same address!
    User user2 = new User(1, address);
    realm.copyToRealmOrUpdate(user2);

    realm.commitTransaction();
    realm.close();
  }
}
